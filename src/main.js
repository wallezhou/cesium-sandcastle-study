import Vue from 'vue'
import App from './App.vue'
import "@/lib/D3Kit/libs/cesium-d3kit.js"

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
