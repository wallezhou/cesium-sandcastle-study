# cesium-sandcastle-study
```
所有示例均在views文件下以vue组件的形式存在，直接在App.vue中修改引入组件即可预览不同示例
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

